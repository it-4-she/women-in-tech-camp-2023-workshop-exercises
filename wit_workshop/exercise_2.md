# Exercise 2

1. Create a branch `exercise_2` from `master` branch.
2. Go to `Build -> Pipeline editor` and push `Configure pipeline` button.
3. Let’s edit the pipeline, so it’s going to:
   1. have a `deploy` stage and `deploy-job` job renamed to `upload` and `upload-job` respectively.
   2. have changed order of jobs execution. Put `test` stage in the beginning of the list.
   3. have a `image: python:3.11.6` section in the pipeline configuration.
   4. have a `before_script` section with commands that you think will help you during debugging process.
   5. have a `saas-linux-medium-amd64` runner machine used for running `unit-test-job` job.
   6. you might want to clone repo locally and try to run tests and build packages (sdist and bdist). Save commands
   used during the process.
   7. run unit tests (using pytest) on a class `TestSymbolic` in the `unit-test-job` job.
   8. create a binary package out of our code and save it to a package registry during `upload-job` job.
   How can you use packages created during `build` stage?
4. Commit your changes and go to `Build -> Pipelines` to see if the pipeline succeeded.
5. If job(s) failed, open a given job and try to debug it.
6. Finally, create `exercise_2_2` branch, and:
   1. add a rule to your pipeline configuration that is going to disable a pipeline run after every commit and force
   pipeline run only on a merge request. 
   2. commit and check if the pipeline ran. If not, create a merge request to the `exercise_2` branch and check that the 
   pipeline ran this time. 
7. Merge branch `exercise_2_2` into `exercise_2` branch and then remaining one into `master` branch.

### Useful links:

- [job-control](https://docs.gitlab.com/ee/ci/jobs/job_control.html)
- [merge-request-pipelines](https://docs.gitlab.com/ee/ci/pipelines/merge_request_pipelines.html)
- [build-PyPI-package](https://docs.gitlab.com/ee/user/packages/workflows/build_packages.html#create-a-pypi-package)
- [publish-generic-package-CI/CD](https://docs.gitlab.com/ee/user/packages/generic_packages/index.html#publish-a-generic-package-by-using-cicd)
- [main-README](../README.md)


                   			           GREAT JOB! YOU’VE MADE IT AGAIN! 
